package ru.dvm.echoserver;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

public class Client implements Runnable{
	
	static public Socket connection;
	Boolean serverRun;
	
	public static void main(String[] args) {
			new Thread(new Client()).start();
	}

	public Client(){
		System.out.println("Welcome to Client");
	}
	
	@Override
	public void run() {
		try {
			connection = new Socket(InetAddress.getByName("127.0.0.1"), 1111);          
            DataInputStream input = new DataInputStream(connection.getInputStream());
            DataOutputStream output = new DataOutputStream(connection.getOutputStream());
            BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
            
            String message = null;            		
			while(true){				
				message = keyboard.readLine();
				output.writeUTF(message);
				output.flush(); 
				message = input.readUTF();
                System.out.println("������ ������: " + message);
                logWriter(message);
			}
		
		} catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void logWriter(String message) {
		  try(FileWriter writer = new FileWriter("log/ChatLog.txt", true))
	        {	
	            writer.write(message + System.lineSeparator());
	            writer.flush();
	        }
	        catch(IOException e){
	        	 System.out.println(e.getMessage());
	        } 
	}	

}
